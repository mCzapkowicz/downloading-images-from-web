package com.example.michael.internet;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.InputStream;
import java.net.URL;

public class ImageActivity extends AppCompatActivity {

    private ImageView img;
    private TextView txt;
    private ProgressBar prgs;
    private String address = "http://img00.deviantart.net/0c6a/i/2012/242/1/f/png_moon_by_paradise234-d5czhdo.png";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        img = (ImageView)findViewById(R.id.imageView);
        prgs = (ProgressBar) findViewById(R.id.progressBar);
        txt = (TextView) findViewById(R.id.textView2);

        Bundle b = getIntent().getExtras();
        String adr = b.getString("address");
        ImageTransfer it = new ImageTransfer();
        it.execute(adr);
    }

    class ImageTransfer extends AsyncTask<String, Void, Boolean> {
        private Bitmap bmp;

        @Override
        protected void onPreExecute() {
            prgs.setVisibility(ProgressBar.VISIBLE);
            txt.setText("Image tranfer in progress...");
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Boolean bRes) {
            if(bRes) {
                Canvas cnv = new Canvas(bmp);
                Paint pen = new Paint();
                int w = bmp.getWidth();
                int h = bmp.getHeight();
                pen.setColor(Color.BLUE);
                pen.setStyle(Paint.Style.STROKE);
                cnv.drawRect(0,0,w-1,h-1,pen);
                img.setImageBitmap(bmp);
                txt.setText("Transfer complete: image: " + w + "x" + h);

            } else {
                txt.setText("Image transfer error");
            }

            prgs.setVisibility(ProgressBar.INVISIBLE);
            super.onPostExecute(bRes);
        }

        @Override
        protected Boolean doInBackground(String... params) {
            URL url;
            InputStream istr;
            boolean bRet = true;
            try {
                url = new URL(params[0]);
                istr = url.openStream();
                Bitmap tmp = BitmapFactory.decodeStream(istr);
                bmp = tmp.copy(Bitmap.Config.ARGB_8888, true);
            } catch (Exception e) {
                bRet = false;
            }
            return bRet;
        }
    }

}
