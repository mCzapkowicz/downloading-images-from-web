package com.example.michael.internet;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private String img1Url = "http://img00.deviantart.net/0c6a/i/2012/242/1/f/png_moon_by_paradise234-d5czhdo.png";
    private String img2Url = "https://i.pinimg.com/originals/f5/1d/08/f51d08be05919290355ac004cdd5c2d6.png";
    private String img3Url = "https://beautifulcoolwallpapers.files.wordpress.com/2011/07/green-nature-wallpaper1.jpg";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.commonmenu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent act;
        switch (item.getItemId()) {
            case R.id.menu_pic1:
                Toast.makeText(this,"Obraz 1", Toast.LENGTH_SHORT).show();
                act = new Intent(this, ImageActivity.class);
                act.putExtra("address", img1Url);
                startActivity(act);
                return true;
            case R.id.menu_pic2:
                Toast.makeText(this,"Obraz 2", Toast.LENGTH_SHORT).show();
                act = new Intent(this, ImageActivity.class);
                act.putExtra("address", img2Url);
                startActivity(act);
                return true;
            case R.id.menu_pic3:
                Toast.makeText(this,"Obraz 3", Toast.LENGTH_SHORT).show();
                act = new Intent(this, ImageActivity.class);
                act.putExtra("address", img3Url);
                startActivity(act);
                return true;
            case R.id.menu_off:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
